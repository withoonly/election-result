<?php

/*
Author : Sunil Bhatt
*/

class Connection
{
    private $host = "mysql-57-centos7";
    private $user = "root";
    private $password = "playwork";
    private $database = "election-result";
    private $conn;

    function __construct()
    {
        $this->conn = $this->connectDB();
    }

    function connectDB()
    {
        $conn = mysqli_connect($this->host, $this->user, $this->password, $this->database);
        if (mysqli_connect_errno()) {
            echo "Failed to connect to MySQL: " . mysqli_connect_error();
        }
        /* change character set to utf8 */
        if (!mysqli_set_charset($conn, "utf8")) {
            printf("Error loading character set utf8: %s\n", mysqli_error($conn));
        }
        return $conn;
    }

    function runQuery($query)
    {
        $result = mysqli_query($this->conn, $query);
        while ($row = mysqli_fetch_assoc($result)) {
            $resultset[] = $row;
        }
        if (!empty($resultset))
            return $resultset;
    }

    function numRows($query)
    {
        $result = mysqli_query($this->conn, $query);
        $rowcount = mysqli_num_rows($result);
        return $rowcount;
    }
}
