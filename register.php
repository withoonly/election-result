<?php
require_once("dbclass.php");
session_start();

if (isset($_POST["Submit"])) {
    if (empty($_POST["username"]) || empty($_POST["password"])) {
        echo '<script>alert("Both Fields are required")</script>';
    } else {
        $dbConnection = new Connection();
        $conn = $dbConnection->connectDB();
        $username = mysqli_real_escape_string($conn, $_POST["username"]);
        $password = mysqli_real_escape_string($conn, $_POST["password"]);
        $password = password_hash($password, PASSWORD_DEFAULT);
        $query = "INSERT INTO users(username, password) VALUES('$username', '$password')";
        if (mysqli_query($conn, $query) or die(mysqli_error($conn))) {
            $_SESSION["username"] = $username;
            $_SESSION['Active'] = true;
            header("location:index.php");
        }
    }
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="css/signin.css">
    <title>Sign in</title>
</head>
<body>
<br/><br/>
<div class="container" style="width:500px;">
    <br/>
    <form action="" method="post" name="Register_Form" class="form-signin">
        <h2 class="form-signin-heading">Register</h2>
        <label for="inputUsername" class="sr-only">Username</label>
        <input name="username" type="username" id="username" class="form-control" placeholder="Username" required
               autofocus>
        <label for="inputPassword" class="sr-only">Password</label>
        <input name="password" type="password" id="password" class="form-control" placeholder="Password" required>
        <button name="Submit" value="Register" class="btn btn-lg btn-primary btn-block" type="submit">Register</button>
        <a name="Login" class="btn btn-lg btn-primary btn-block" href="login.php">Log in</a>
    </form>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js"></script>
</body>
</html>