<?php
require_once("dbclass.php");
session_start();
/* Check if login form has been submitted */
if (isset($_POST['Submit'])) {
    if (empty($_POST["username"]) || empty($_POST["password"])) {
        header("location:login.php");
    }
    $dbConnection = new Connection();
    $conn = $dbConnection->connectDB();
    $username = mysqli_real_escape_string($conn, $_POST['username']);
    $password = mysqli_real_escape_string($conn, $_POST['password']);
    $result = mysqli_query($conn, "SELECT * FROM users WHERE  username='$username'");
    if (mysqli_num_rows($result) > 0) {
        while ($row = mysqli_fetch_array($result)) {
            if (password_verify($password, $row["password"])) {
                $_SESSION["username"] = $username;
                $_SESSION['Active'] = true;
                header("location:index.php");
            } else {
                echo '<script>alert("Incorrect username or password")</script>';
            }
        }
    } else {
        echo '<script>alert("Incorrect username or password")</script>';
    }
}
?>

<!-- HTML code for Bootstrap framework and form design -->

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css?family=Sarabun:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
          crossorigin="anonymous">

    <link rel="stylesheet" href="./fontello/css/fontello.css">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">

    <link rel="stylesheet" type="text/css" href="css/signin.css">
    <title>Sign in</title>
</head>
<body>
<div class="container-fluid p-0">
    <div class="col-6 p-0 float-left">
        <img class="form-login-img" src="images/bg.png"/></div>
    <div class="col-6 p-0 float-left ">

        <div class="col-6 form-login-wrapper">

            <h4 class="mb-10 font-weight-bold">เข้าสู่ระบบ</h4>

            <form action="" method="post" class="form-login">


                <div class="form-group ">
                    <label class="font-weight-bold mb-3" for="exampleInputEmail1">
                        เลขประจำตัว
                    </label>

                    <div class="input-group">
                        <i class="fas icon-userid  mt-2"></i>
                        <input name="username" type="text" class="form-control" placeholder="เลขประจำตัว">
                    </div>

                </div>
                <div class="form-group mt-4">
                    <label class="font-weight-bold mb-3"" for="exampleInputPassword1">
                    รหัสผ่าน 6 หลัก
                    </label>
                    <div class="input-group">
                        <i class="fas icon-lock  mt-2"></i>
                        <input name="password" type="password" class="form-control" placeholder="รหัสผ่าน">
                    </div>
                </div>
                <div class="row justify-content-center mt-5">
                    <button name="Submit" type="submit" class="btn btn-primary  form-login-btn ">เข้าสู่ระบบ</button>
                </div>
            </form>
        </div>

    </div>

</div>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script>
    if (window.history.replaceState) {
        window.history.replaceState(null, null, window.location.href);
    }
</script>
</body>
</html>
