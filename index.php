<?php
session_start(); /* Starts the session */
if ($_SESSION['Active'] == false) { /* Redirects user to Login.php if not logged in */
    header("location:login.php");
    exit;
}
require_once("dbclass.php");
require_once("pagination.class.php");
$dbConnection = new Connection();
$perPage = new Pagination();

$page = 1;
if (!empty($_GET["page"])) {
    $page = $_GET["page"];
}

$start = ($page - 1) * $perPage->perpage;
if ($start < 0) $start = 0;

$sqlQuery = "SELECT
	results_2562.id,
	party.name,
    party.logo,
	results_2562.khet_list,
	results_2562.party_list,
    results_2562.khet_list + results_2562.party_list total
FROM
	results_2562
JOIN party ON party.code = results_2562.party_id";

$query = $sqlQuery . " limit " . $start . "," . $perPage->perpage;
$getData = $dbConnection->runQuery($query);

$rowcount = $dbConnection->numRows($sqlQuery);
$url = 'http://' . $_SERVER['SERVER_NAME'];
$showPagination = $perPage->getAllPageLinks($url, $rowcount, $page, "page-item", "link");
//$getData=json_decode("[
//  {
//    \"id\": 5,
//    \"name\": \"พรรคประชาธิปัตย์\",
//    \"logo\": \"/images/party/party_02.jpg\",
//    \"khet_list\": 34,
//    \"party_list\": 0,
//    \"total\": 34
//  },
//  {
//    \"id\": 3,
//    \"name\": \"พรรคเพื่อไทย\",
//    \"logo\": \"/images/party/party_01.jpg\",
//    \"khet_list\": 135,
//    \"party_list\": 0,
//    \"total\": 135
//  },
//  {
//    \"id\": 6,
//    \"name\": \"พรรคชาติพัฒนา\",
//    \"logo\": \"/images/party/party_20.jpg\",
//    \"khet_list\": 1,
//    \"party_list\": 0,
//    \"total\": 1
//  },
//  {
//    \"id\": 7,
//    \"name\": \"พรรคชาติไทยพัฒนา\",
//    \"logo\": \"/images/party/party_03.jpg\",
//    \"khet_list\": 7,
//    \"party_list\": 0,
//    \"total\": 7
//  },
//  {
//    \"id\": 4,
//    \"name\": \"พรรคภูมิใจไทย\",
//    \"logo\": \"/images/party/party_04.jpg\",
//    \"khet_list\": 39,
//    \"party_list\": 0,
//    \"total\": 39
//  },
//  {
//    \"id\": 2,
//    \"name\": \"พรรคอนาคตใหม่\",
//    \"logo\": \"/images/party/party_10.jpg\",
//    \"khet_list\": 29,
//    \"party_list\": 0,
//    \"total\": 29
//  },
//  {
//    \"id\": 8,
//    \"name\": \"พรรครวมพลังประชาชาติไทย\",
//    \"logo\": \"/images/party/party_13.jpg\",
//    \"khet_list\": 1,
//    \"party_list\": 0,
//    \"total\": 1
//  },
//  {
//    \"id\": 9,
//    \"name\": \"พรรคประชาชาติ\",
//    \"logo\": \"/images/party/party_15.jpg\",
//    \"khet_list\": 6,
//    \"party_list\": 0,
//    \"total\": 6
//  },
//  {
//    \"id\": 1,
//    \"name\": \"พรรคพลังประชารัฐ\",
//    \"logo\": \"/images/party/party_14.jpg\",
//    \"khet_list\": 98,
//    \"party_list\": 18,
//    \"total\": 116
//  }
//]",true);
?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>PHP Pagination with Bootstrap</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="https://fonts.googleapis.com/css?family=Kanit:400,700&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.6.3/css/all.css"
          integrity="sha384-UHRtZLI+pbxtHCWp1t77Bi1L4ZtiqrqD80Kn4Z8NTSRyMA2Fd33n5dQ8lWUE00s/"
          crossorigin="anonymous">

    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link rel="stylesheet" type="text/css" href="css/index.css">

</head>
<body>
<!-- <nav class="navbar fix-top" style="background-color:#4061A6">

    <div class="navbar-nav"> <p class="float-right">สรุปจำนวน สส.</p></div>

</nav>
 -->

<div class="row">
    <div class="col-12 p-3 top-content"  >
    
    <div class=" float-right p-1 top-content-text-warp">
       <h6 class="p-0 pb-1 top-content top-content-text" >สรุปจำนวน สส.</h6>
       <hr  class="line"/>
   </div>

</div>


</div>

<main class="container-fluid table-wrapper">

  <div class="text-header-warpper">
        <h3 class="title-text">สรุปจำนวน</h3>
        <h3 class="title-text">สมาชิกสภาผู้แทนราษฎร</h3>
  </div>

    <table class="table  table-borderless party-table" >
        <thead class="party-thead" >
        <tr>
            <th scope="col">
                <img class="party-table-tag"  src="./images/tag.png" >
            </th>
            <th class="header-text pl-0" scope="col">
                <h5>พรรคการเมือง</h5>
            </th>
            <th class="header-text"  scope="col">
                <h5 class="float-right">จำนวน <i class="fas fa-circle dot-blue"></i> </h5>
            </th>
            <th class="header-text"  scope="col">
                <h5 class="float-right">แบ่งเขต <i class="fas fa-circle dot-red "></i></h5>
            </th>
            <th class="header-text" scope="col">
                <h5 class="float-right">ปาร์ตี้ลิสต์ <i class="fas fa-circle dot-green "></i></h5>
            </th>
            <th scope="col">&nbsp</th>
        </tr>
        </thead>
        <tbody>
        <?php
        foreach ($getData as $data) {
            echo "<tr>
                  <td></td>
                  <td>
                    <img  class='img-fluid img-thumbnail party-img float-left' src=" . $data["logo"] . ">
                     <p class='party-text font-weight-bold '>" . $data["name"] . "</p>  
                   </td>
                  <td> <p class='party-text float-right'>" . $data["total"] . "</p></td>
                  <td> <p class='party-text float-right'>" . $data["khet_list"] . "</p></td>
                  <td> <p class='party-text float-right'>" . $data["party_list"] . "</p></td>
                  <td></td>
                  <tr>";
        }
        ?>
        </tbody>
        <tfoot>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td>&nbsp;</td>

        </tr>
        </tfoot>
    </table>
    <div id="pagination-result">

       <!-- <nav class="float-right">
            <ul class="pagination">
                <li class="page-item">
                <a class="page-link" href="#" aria-label="Previous">
                    <span aria-hidden="true">&laquo;</span>
                    <span class="sr-only">Previous</span>
                </a>
                </li>
                <li class="page-item"><a class="page-link" href="#">1</a></li>
                <li class="page-item"><a class="page-link" href="#">2</a></li>
                <li class="page-item"><a class="page-link" href="#">3</a></li>
                <li class="page-item">
                <a class="page-link" href="#" aria-label="Next">
                    <span aria-hidden="true">&raquo;</span>
                    <span class="sr-only">Next</span>
                </a>
                </li>
            </ul>
        </nav> -->
        <?php
//        if (!empty($showPagination)) {
            ?>
        <div class="col-12 ">
            <ul class="pagination float-right">
                <?php echo $showPagination; ?>
            </ul>
        </div>

            <?php
//        }
        ?>
    </div>
</main>

<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
</body>
</html>
